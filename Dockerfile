FROM ubuntu:18.04

MAINTAINER Aleksey Chirkin <a4irkin@gmail.com>

RUN echo "===> Installing Python3..."  && \
    apt-get update && apt-get install -y python3 python3-pip

RUN echo "===> Installing Ansible..."  && \
    pip3 install ansible

RUN echo "===> Installing handy tools (not absolutely required)..."  && \
    pip3 install --upgrade pycrypto pywinrm     && \
    apt-get install -y sshpass openssh-client git rsync

RUN echo "===> Adding hosts for convenience..."  && \
    mkdir /etc/ansible && \
    echo 'localhost' > /etc/ansible/hosts

# default command: display Ansible version
CMD [ "ansible-playbook", "--version" ]
